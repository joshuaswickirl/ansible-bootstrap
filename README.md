# Ansible Bootstrap

### Linux
```bash
curl -s https://gitlab.com/joshuaswickirl/ansible-bootstrap/raw/master/bootstrap-ansible-linux.sh | bash
```

### Windows
1. Enter PowerShell
```cmd
powershell
```
2. Download Bootstrap for Windows
```ps1
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
Invoke-WebRequest `
    -uri "https://gitlab.com/joshuaswickirl/ansible-bootstrap/raw/master/bootstrap-ansible-windows.ps1" `
    -OutFile "./bootstrap-ansible-windows.ps1"
```
3. Run Bootstrap for Windows
```ps1
powershell.exe -ExecutionPolicy Bypass -File "./bootstrap-ansible-windows.ps1"
```

[Windows User Guide](https://docs.ansible.com/ansible/latest/user_guide/windows.html)

[Windows Setup Official Documentation](https://docs.ansible.com/ansible/latest/user_guide/windows_setup.html#windows-ssh-setup)

[Install Win32 OpenSSH](https://github.com/PowerShell/Win32-OpenSSH/wiki/Install-Win32-OpenSSH)
