#
#   Bootstrap Ansible on Windows
#

# Set SSH Version
$OpenSSHVersion = "8.0.0.0p1-Beta"


# Download and install OpenSSH
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
Invoke-WebRequest `
    -uri "https://github.com/PowerShell/Win32-OpenSSH/releases/download/v${OpenSSHVersion}/OpenSSH-Win64.zip" `
    -OutFile "C:\Windows\Temp\OpenSSH-Win64.zip"

Expand-Archive `
    -LiteralPath "C:\Windows\Temp\OpenSSH-Win64.zip" `
    -DestinationPath "C:\Program Files\"

Rename-Item `
    -LiteralPath "C:\Program Files\OpenSSH-Win64" `
    -NewName "OpenSSH"

powershell.exe `
    -ExecutionPolicy "Bypass" `
    -File "C:\Program Files\OpenSSH\install-sshd.ps1"

# Open firewall for sshd.exe
New-NetFirewallRule `
    -Name "sshd" `
    -DisplayName "OpenSSH Server (sshd)" `
    -Enabled True `
    -Direction "Inbound" `
    -Protocol "TCP" `
    -Action "Allow" `
    -LocalPort 22

# Start & enable sshd
Start-Service `
    -Name sshd

Set-Service "sshd" `
    -StartupType "Automatic"

# Make PowerShell default shell
New-ItemProperty `
    -Path "HKLM:\SOFTWARE\OpenSSH" `
    -Name DefaultShell `
    -Value "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" `
    -PropertyType String `
    -Force

 New-ItemProperty `
    -Path "HKLM:\SOFTWARE\OpenSSH" `
    -Name DefaultShellCommandOption `
    -Value "/c" `
    -PropertyType String `
    -Force

# Create ansible user and grand admin rights
Write-Host "Creating Ansible user, enter a secure password."
New-LocalUser `
    -Name "ansible" `
    -AccountNeverExpires

Add-LocalGroupMember `
    -Group "Administrators" `
    -Member "ansible"

# Prompt user to access ansible user
$IPv4Address = (
    Get-NetIPConfiguration |
    Where-Object {
        $_.IPv4DefaultGateway -ne $null -and
        $_.NetAdapter.Status -ne "Disconnected"
    }
).IPv4Address.IPAddress

Write-Host "Ansible user created."
while ( !(Test-Path "C:\Users\ansible") ) {
    Write-Host "Access this server using 'ssh ansible@${IPv4Address}' to create user profile. Waiting..."
    Start-Sleep -Seconds 15
}

# Prepare authorized_key file
New-Item `
    -Path "C:\Users\ansible\.ssh" `
    -ItemType "Directory" `
    -Force

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
Invoke-WebRequest `
    -uri "https://gitlab.com/snippets/1859133/raw" `
    -OutFile "C:\Users\ansible\.ssh\authorized_keys"

# Run permissions cleanup script
powershell.exe `
    -ExecutionPolicy "Bypass" `
    -File "C:\Program Files\OpenSSH\FixHostFilePermissions.ps1"

# Update sshd_config and restart the sshd service
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
Invoke-WebRequest `
    -uri "https://gitlab.com/joshuaswickirl/ansible-bootstrap/raw/master/windows_sshd_config" `
    -OutFile "C:\ProgramData\ssh\sshd_config"

Restart-Service `
    -Name "sshd"


Write-Host "Ansible bootstrap complete."
