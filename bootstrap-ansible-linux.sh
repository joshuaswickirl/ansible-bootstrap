#!/bin/bash

#
#   Bootstrap Ansible on Linux
#


# SSH Public Key URL
pub_key_url="https://gitlab.com/snippets/1859133/raw"


if [ $(whoami) != root ]; then
    echo "Ansible bootstrap must be ran as root user."
    exit 0
fi

# create ansible system user
if [ ! $(getent passwd ansible) ]; then
    useradd -m -s /bin/bash ansible
fi

# grant ansible sudo rights
echo "ansible ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/ansible
chmod 0440 /etc/sudoers.d/ansible

# add public key to /home/ansible/.ssh/authorized_keys
mkdir -p /home/ansible/.ssh/
curl $pub_key_url -s -o /home/ansible/.ssh/authorized_keys
chown -R ansible:ansible /home/ansible/.ssh/

echo "Ansible bootstrap complete."